﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanCoding.Core.Entities
{
    public class Education : Audit
    {

        public string SchoolAttended { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Discipline { get; set; }
        public string Degree { get; set; }
        public string Grade { get; set; }
    }
}
