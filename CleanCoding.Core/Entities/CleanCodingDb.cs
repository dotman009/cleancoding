﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanCoding.Core.Entities
{
    public class CleanCodingDb : DbContext
    {
        public CleanCodingDb(DbContextOptions<CleanCodingDb> options):base(options)
        {

        }
        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Education> Educations { get; set; }
    }
}
