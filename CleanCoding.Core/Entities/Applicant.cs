﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanCoding.Core.Entities
{
    public class Applicant : Audit
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string RegNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public int EducationId { get; set; }
        public virtual Education Education { get; set; }
    }
}
