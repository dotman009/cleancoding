﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanCoding.Core.Dto
{
    public class ApplicantDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string RegNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
    }
}
