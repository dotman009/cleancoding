﻿using AutoMapper;
using CleanCoding.Core.Dto;
using CleanCoding.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanCoding.Core.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Applicant, ApplicantDto>();
        }
    }
}
