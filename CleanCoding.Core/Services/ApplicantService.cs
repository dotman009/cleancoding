﻿using AutoMapper;
using CleanCoding.Core.Dto;
using CleanCoding.Core.Entities;
using CleanCoding.Core.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanCoding.Core.Services
{
    public class ApplicantService : IApplicant
    {
        private readonly CleanCodingDb _db;
        private readonly IMapper _mapper;
        public ApplicantService(CleanCodingDb db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }
        public async Task<bool> Register(Applicant applicant)
        {
            try
            {
                var request = new Applicant
                {
                    BirthDate = applicant.BirthDate,
                    CreatedBy = null,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    EducationId = applicant.EducationId,
                    FirstName = applicant.FirstName,
                    Gender = applicant.Gender,
                    IsCreated = true,
                    IsModified = false,
                    LastName = applicant.LastName,
                    ModifiedBy = null,
                    RegNumber = null,
                    UserName = applicant.UserName,
                };
                _db.Applicants.Add(request);
                await _db.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
