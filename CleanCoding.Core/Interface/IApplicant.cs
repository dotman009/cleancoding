﻿using CleanCoding.Core.Dto;
using CleanCoding.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanCoding.Core.Interface
{
    public interface IApplicant
    {
        Task<bool> Register(Applicant applicant);
    }
}
